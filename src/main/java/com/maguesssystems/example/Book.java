package com.maguesssystems.example;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



/**
 * Hassan Serhan
 *
 */

public class Book 
{  

	public Book(String title, String authorname, Integer pagecount, Integer yearOfRelease) {
		super();
		this.title = title;
		this.authorname = authorname;
		this.pagecount = pagecount;
		this.yearOfRelease = yearOfRelease;
	}

	public static void main( String[] args )
    {
        System.out.println( "Aufgabe 3" );
        bookGroupedByAuthorSortedByPagecount(sampleBookList(),1890,1950);
    }
    
    private  String title;
    private  String  authorname;
    private  int pagecount;
    private  int yearOfRelease;
    
    
    
    public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthorName() {
		return authorname;
	}

	public void setAuthorName(String authorname) {
		this.authorname = authorname;
	}

	public int getPageCount() {
		return pagecount;
	}

	public void setPageCount(int pagecount) {
		this.pagecount = pagecount;
	}

	public int getYearOfRelease() {
		return yearOfRelease;
	}

	public void setYearOfRelease(int yearOfRelease) {
		this.yearOfRelease = yearOfRelease;
	}
	
	

	@Override
	public String toString() {
		return "Book [authorname=" + authorname + "title=" + title +  ", pagecount=" + pagecount + ", yearOfRelease="
				+ yearOfRelease + "]";
	}

	public static List<Book> sampleBookList(){
    	List<Book> list = new ArrayList<>();
    	list.add(new Book("The Restaurant at the End of Universe","Douglas Adams",334,1980));
    	list.add(new Book("Winnetou","Karl May",520,1893));
    	list.add(new Book("So long, And thanks for all the Fish","Douglas Adams",290,1984));
    	list.add(new Book("Dreigroschenoper","Bertrold Brecht",211,1928));
    	list.add(new Book("Schatz am Silbersee","Karl May",420,1890));
    	list.add(new Book("Artemis","Andy Weir",367,2017));
    	list.add(new Book("Buddenbrooks","Thomas Mann",238,1901));
    	list.add(new Book("The Salmon of Doubt","Douglas Adams",150,2003));
    	list.add(new Book("The Martian","Andy Weir",240,2011));
    	list.add(new Book("Mostly Harmless","Douglas Adams",432,1992));   
    	list.add(new Book("Der Zauberberg","Thomas Mann",155,1924));  
    	
		return list;
    	
    }
	
	
	public static List<Book> bookGroupedByAuthorSortedByPagecount(List<Book> unsortedList, int minYearOfRelease, int maxYearOfRelease){	
	
	Map<String, List<Book>> result = unsortedList.stream()
	        .collect(Collectors.groupingBy(Book::getAuthorName, Collectors.toList()));

	// sort groups 
	List<Book> sorted = result.entrySet().stream()
	        .sorted(Comparator.comparing(e -> e.getValue().stream().map(Book::getPageCount).min(Comparator.naturalOrder()).orElse(0)))
	//then sort every group  
	        .flatMap(e -> e.getValue().stream()
            .filter(age -> age.getYearOfRelease() >= minYearOfRelease && age.getYearOfRelease() <= maxYearOfRelease )
            .sorted(Comparator.comparing(Book::getPageCount)))
            .collect(Collectors.toList());
	
	//sorted.forEach(e -> System.out.println("Authorname:"+ e.getAuthorName()+", pageCount: "+e.getPageCount()+", Title:" +e.getTitle()+", yearOfRelease:"+e.getYearOfRelease()));
	
	sorted.forEach(e -> System.out.println(e));
	
	return sorted;
	
	}

}
